﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour {

	//public Vector2 velocity = new Vector2(-4, 0);
//	public float range = 4;

	// Use this for initialization
	void Start()
	{
		//        AdmobMan.Instance.ShowBanner();
		//GetComponent<Rigidbody2D>().velocity = velocity;
		float xPos= GameObject.Find("player").transform.position.x;
		float yPos=  GameObject.Find("player").transform.position.y;
		float zPos= GameObject.Find("player").transform.position.z;
		//Debug.Log ("xPos = " + xPos);
		transform.position = new Vector3(xPos, yPos , zPos);

	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
		//	Debug.Log ("Escape in Obstacle : ");
			Time.timeScale = 0;
		} else {
		//	Debug.Log("Running in Obstacle : ");
			Time.timeScale = 1;
		}

		Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		//  Debug.Log("screen.width: " + Screen.width);
		//   Debug.Log("x: " + screenPosition.x);
		if (screenPosition.x > Screen.width)
		{
			//  Debug.Log("screen.width: " + Screen.width);
			//Debug.Log("x: " + screenPosition.x);
			Destroy(GameObject.Find("Scripts"));
		}
	}
}
