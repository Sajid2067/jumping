﻿using UnityEngine;
using UnityEngine.UI;

public class Generate : MonoBehaviour
{
  //  public GUIText scoreText;
    public GameObject rocks;

	public Text txtScore;

	float score ;

    // Use this for initialization
    void Start()
    {
		score = 0;
        InvokeRepeating("CreateObstacle", 1f, 1.5f);

		txtScore = txtScore.GetComponent<Text> ();
		txtScore.text = "Score: "+score;
	

    }


	// Update is called once per frame
	void Update () {

		if(Input.GetKeyUp("escape")){
		//	Debug.Log ("Escape in generate : ");
			Time.timeScale = 0;
		} else {
		//	Debug.Log("Running in generate : ");
		}
	//	txtScore = "Score : " + score.ToString ();

//		if (Input.GetKeyUp (KeyCode.Escape)||Input.GetKeyUp (KeyCode.P)) {
//			Debug.Log("Escaped : ");
//			Time.timeScale = 0;
//		} else {
//			Time.timeScale = 1;
//		}
//		Debug.Log("txtScore.IsActive : "+txtScore.IsActive);
//		if (txtScore.IsActive) {
//			Debug.Log ("Active : ");
//		} else {
//			Debug.Log ("Not Active : ");
//		}



	}

	private void OnHideUnity(bool isGameShown){

		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}

	}


 

    void CreateObstacle()
    {
        Instantiate(rocks);
        score++;
		txtScore.text = "Score: "+score;

		PlayerPrefs.SetFloat("score",score);
		if(PlayerPrefs.GetFloat("highestScore")<score){
			PlayerPrefs.SetFloat("highestScore", score);
		}
	//	Debug.Log("Score : ", score);
    }


}