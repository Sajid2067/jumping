﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Facebook.Unity;
using System.IO;


public class Menu : MonoBehaviour {

	public Button btnYes;
	public Button btnNo;
	public Button btnFacebook;
	public Button btnShareLink;
	public Text txtHighest;
	public Text txtScore;
	public float highestScore=0;

	private bool isProcessing = false;
	//public Image buttonShare;
	public string link="https://play.google.com/store/apps/details?id=com.Honesty.FlyingPlane";
	// Use this for initialization
	void Start () {

		AdmobMan.Instance.ShowBanner ();

		btnYes = btnYes.GetComponent<Button> ();
		btnNo = btnNo.GetComponent<Button> ();
		btnFacebook = btnFacebook.GetComponent<Button> ();
		btnShareLink = btnShareLink.GetComponent<Button> ();
		//	txtHighestScore = txtHighestScore.GetComponent<Text> ();
	//	if(PlayerPrefs.GetInt("highestScore")<score){
	//		highestScore=PlayerPrefs.SetInt("highestScore",score);
	//	}
		txtScore.text = "Score : "+PlayerPrefs.GetFloat("score");
		txtHighest.text = "Highest Score : "+PlayerPrefs.GetFloat("highestScore");
		//	txtHighestScore = txtHighestScore.GetComponent<Text> ();


		btnYes.onClick.AddListener (yesPress);
		btnNo.onClick.AddListener (noPress);
		btnFacebook.onClick.AddListener (Share);
		btnShareLink.onClick.AddListener (linkShare);



	}

	// Update is called once per frame
	void Update () {

	}

	public void yesPress(){
		SceneManager.LoadScene ("scene001");
	}

	public void noPress(){
		Application.Quit ();
	}

	public void facebookPress(){
		//LogIn ();
		Share();
	}

	public void linkShare(){
		StartCoroutine( ShareLink() );
	}

	public IEnumerator ShareLink(){

		yield return new WaitForEndOfFrame();

		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
	//	AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","file://" + destination);
	//	intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

		intentObject.Call<AndroidJavaObject> ("setType", "text/plain");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), link);
		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "test");
		//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");

		//	intentObject.Call<AndroidJavaObject>("getData").Call<string>("toString");
		//	intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "test");


		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		currentActivity.Call("startActivity", intentObject);

	}

	private void Awake(){
		if (!FB.IsInitialized) {
			FB.Init ();
		} else {
			FB.ActivateApp ();
		}

	}

	public void LogIn()
	{
		FB.LogInWithReadPermissions (callback:OnLogIn);
	}

	private void OnLogIn(ILoginResult result){

		if (FB.IsLoggedIn) {
			//AccessToken token = AccessToken.CurrentAccessToken;
			//txtScore.text=token.UserId;
			Share();
		} else {
			//Debug.Log ("Cancelled Login");
		}
	}

	public void Share()
	{

		btnFacebook.enabled = false;
		if(!isProcessing){
			StartCoroutine( ShareScreenshot() );
		}
		//Debug.Log ("Share called");
//		FB.FeedShare (
//			contentTitle: "Jumping Plane",
//			contentURL: new System.Uri("www.google.com"),
//			contentDescription: "My Highest Score : "+PlayerPrefs.GetFloat("highestScore"),
//			callback:OnShare);
//		FB.ShareLink(
//			new System.Uri("https://developers.facebook.com/"),
//			callback: OnShare);
//		FB.ShareLink (
//			
//			new System.Uri("https://play.google.com/store/"),
//
//			callback:OnShare);
	}

	private void OnShare(IShareResult result){

		if (result.Cancelled || !string.IsNullOrEmpty (result.Error)) {
			//Debug.Log (result.Error);
			//txtScore.text = result.Error;

		} else if (!string.IsNullOrEmpty (result.PostId)) {
			//Debug.Log (result.PostId);
		//	txtScore.text = result.PostId;
		} else {
			//Debug.Log ("Share successfully");
			//txtScore.text = "successfully";
		}
	}

	public IEnumerator ShareScreenshot()
	{
		isProcessing = true;
		// wait for graphics to render
		yield return new WaitForEndOfFrame();
		//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
		// create the texture
	
		Texture2D screenTexture = new Texture2D(Screen.width, Screen.height,TextureFormat.RGB24,false);
		// put buffer into texture
		screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height),0,0);
		// apply
		screenTexture.Apply();
	//	----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO
		byte[] dataToSave = screenTexture.EncodeToPNG();
		string destination = Path.Combine(Application.persistentDataPath,System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");
		File.WriteAllBytes(destination, dataToSave);
		if(!Application.isEditor)
		{
		//	FB.ShareLink (new System.Uri("https://play.google.com/store/apps/details?id=com.outfit7.mytalkinghank"),"Flying Plane","My highest score : "+PlayerPrefs.GetFloat("highestScore"),callback:OnShare);


			// block to open the file and share it ------------START
			AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
			AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
			intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
			AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
			AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse","file://" + destination);
			intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

			intentObject.Call<AndroidJavaObject> ("setType", "text/plain");
			//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), ""+ mensaje);
			//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "test");
			//intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "SUBJECT");

		//	intentObject.Call<AndroidJavaObject>("getData").Call<string>("toString");
		//	intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "test");


			intentObject.Call<AndroidJavaObject>("setType", "image/png");
			AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

			currentActivity.Call("startActivity", intentObject);
	
		}
		isProcessing = false;
		btnFacebook.enabled = true;
	}



}
