﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Player : MonoBehaviour
{
	// The force which is added when the player jumps
	// This can be changed in the Inspector window
	public Vector2 jumpForce = new Vector2(0, 285);
	public GameObject fireEffects;
	private int dieCounter=0;
	private int dying=0;

	//	public AudioSource audioSource;
	//	public AudioClip Scored;
	//public GameObject other;

	void Start () {

	}
	// Update is called once per frame
	void Update()
	{
		// Jump
		if (Input.GetMouseButtonDown(0))

		{
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			GetComponent<Rigidbody2D>().AddForce(jumpForce);
		}


		// Die by being off screen
		Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		if (screenPosition.y > Screen.height || screenPosition.y < 0)
		{
			Die();
		}
	}

	// Die by collision
	void OnCollisionEnter2D(Collision2D collision)
	{

		Die();
	}

	void Die()
	{
		//ps.Play ();
		//		audioSource = GetComponent<AudioSource>();
		//		audioSource.clip = Scored;
		//		audioSource.Play ();
		if (dying == 0) {
			dying = 1;
			Instantiate (fireEffects);

			StartCoroutine (ExecuteAfterTime ());
		}

		//    SceneManager.LoadScene(0);
		//   ShowPopUp();

	}
	
	 IEnumerator ExecuteAfterTime()
 {
     yield return new WaitForSeconds(1f);
		dieCounter=PlayerPrefs.GetInt("counter");
		if (dieCounter <= 4) {
			dieCounter++;
		} else {
			dieCounter = 0;
			AdmobMan.Instance.ShowVideo();
		}

		Debug.Log ("DieCounter: "+dieCounter);

		PlayerPrefs.SetInt("counter",dieCounter);
		SceneManager.LoadScene ("sceneMenu");
     // Code to execute after the delay
 }

	// Add 



}