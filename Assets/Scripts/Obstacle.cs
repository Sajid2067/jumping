﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public Vector2 velocity = new Vector2(-4, 0);
    public float range = 4;

    // Use this for initialization
    void Start()
    {
//        AdmobMan.Instance.ShowBanner();
        GetComponent<Rigidbody2D>().velocity = velocity;
        transform.position = new Vector3(transform.position.x, transform.position.y - range * Random.value, transform.position.z);
	
    }

    void Update()
    {
		if (Input.GetKeyDown (KeyCode.Escape)) {
		//	Debug.Log ("Escape in Obstacle : ");
			Time.timeScale = 0;
		} else {
			//Debug.Log("Running in Obstacle : ");
			Time.timeScale = 1;
		}

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
      //  Debug.Log("screen.width: " + Screen.width);
     //   Debug.Log("x: " + screenPosition.x);
        if (screenPosition.x > Screen.width)
        {
          //  Debug.Log("screen.width: " + Screen.width);
          //  Debug.Log("x: " + screenPosition.x);
           Destroy(GameObject.Find("Scripts"));
        }
    }
}