﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {
	public AudioSource audioSource;
	public AudioClip clip;
	// Use this for initialization
	void Start () {
		Debug.Log ("Start =AudioScript " );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		Debug.Log ("OnCollisionEnter2D =AudioScript " );
		audioSource = GetComponent<AudioSource>();
		audioSource.PlayOneShot (clip, 0.7F);

	}
}
